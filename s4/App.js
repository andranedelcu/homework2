import React from 'react';
import './App.css';
import AddProduct from './AddProduct.js';
import ProductList from './ProductList.js';

class App extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {};
    this.state.products = [];
  }
  
  onProductAdded = (product) => {
    let products = this.state.products;
    products.push(product);
    this.setState({
      products:product
    });
  }
  
  // componentWillMount(){
  //   const url = '/get-all'
  //   fetch(url).then((res) => {
  //     return res.json();
  //   }).then((products) =>{
  //     this.setState({
  //       products:products
  //     })
  //   })
  // }
  
  render() {
    return (
      <div className="App">
        <h1>PROIECT</h1>
       
        <ProductList source={this.state.products}/>
        <AddProduct ProducutAdded={this.onProductAdded}/>
        
      </div>
    );
  }
}

export default App;
