import React from 'react';
import axios from 'axios';

class AddProduct extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
        this.state.productName = "";
        this.state.price = "";
    }
    
    handleChangeProductName = (event) => {
        this.setState({
            productName: event.target.value
        });
    }

    
    handleChangePrice = (event) => {
        this.setState({
            price: event.target.value
        });
    }
    
    handleAddClick = () => {
        let product = {
            productName: this.state.productName,
            price: this.state.price
        }
        axios.post('/add', product).then((res) => {
            if(res.status === 200){
                this.props.ProducutAdded(product)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }
    
    render(){
        return(
            <div>
            <h1>Add Product</h1>
                
                <input type="text" placeholder="Product Name" 
                    onChange={this.handleChangeProductName}
                    value={this.state.productName} />
                    
                <input type="text" placeholder="Price" 
                    onChange={this.handleChangePrice}
                    value={this.state.price} />
                    
                <button onClick={this.handleAddClick}>Add Product</button>
            </div>
            );
    }
}

export default AddProduct;
